from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext

PATH2DATA = '/home/commun/echange-EDP/data/SNDS_aplati/'
PROJECT_NAME = 'bigoudi_edp'

SPARK_URL = 'spark://localhost:7077'

def start_sc(
        nb_executors: int = 4,
        local: bool = False,
        arrow_enabled: bool = False,
        executor_memory: str = '32g',
        driver_memory: str = '32g',
        max_result_size: str = '64g',
        parallelism: int = 500,
        conf_only: bool = False):
    """
    # taking inspiration from : https://spoddutur.github.io/spark-notes/distribution_of_executors_cores_and_memory_for_spark_application.html
    bigoudi is 1 node, 80 cores, 2000Go Ram
    Logic:
        - set 4 executors per exec,
        - set 32GB per exec by default,
        - let the user control the number of executors with a 20 limit (20*4=80 cores, 32**2090 heap memory=669 GB)
    :param nb_executors:
    :param local:
    :param arrow_enabled:
    :param executor_memory:
    :param driver_memory:
    :param max_result_size: used for spark actions (eg. collect) or for the driver heap allocation (if you have huged execution graphs)
    :param parallelism:
    :return:
    """

    if local:
        locality = [('spark.master', 'local[{}]'.format(nb_executors * 4))]
    else:
        locality = [
            ('spark.master', SPARK_URL),
            ('spark.executor.cores', '4'),
            ('spark.num.executors', '{}'.format(nb_executors))
        ]
    conf = SparkConf().setAll([
        *locality,
        ('spark.executor.memory', '{}'.format(executor_memory)),
        ('spark.driver.memory', '{}'.format(driver_memory)),
        ('spark.cores.max', '{}'.format(4 * nb_executors)),
        ('spark.sql.shuffle.partitions', '{}'.format(parallelism)),
        ('spark.default.parallelism', '{}'.format(parallelism)),
        ('spark.driver.maxResultSize', '{}'.format(max_result_size)),
        ("spark.sql.execution.arrow.enabled", str(arrow_enabled).lower()),
        ('spark.driver.extraJavaOptions',
         '-Djava.io.tmpdir=/home/commun/echange/matthieu_doutreligne/tmp/'),
        ('spark.executor.extraJavaOptions',
         '-Djava.io.tmpdir=/home/commun/echange/matthieu_doutreligne/tmp/'),
        ('spark.dynamicAllocation.enabled', 'true'),
        ('spark.shuffle.service.enabled', 'true')
    ])
    if conf_only:
        return conf
    sc = SparkContext(conf=conf)
    sql_context = SQLContext(sc)
    return sql_context
