import os
import re
import papermill as pm

QUERIED_TABLES = ['DCIR', 'MCO', 'SSR', 'HAD', 'PSY', 'IRBENR', 'IRIMBR']
PATH2DATA = '/home/commun/echange-EDP/data/SNDS_aplati/'
PROJECT_NAME = 'bigoudi_edp'

notebooks_filenames = [f for f in os.listdir('.') if re.search('.ipynb$', f) is not None]
output_dir = '.'

if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

for notebook in notebooks_filenames:
    pm.execute_notebook(
        notebook,
        os.path.join(output_dir, notebook),
        parameters=dict(
            QUERIED_TABLES=QUERIED_TABLES,
            PATH2DATA=PATH2DATA,
            PROJECT_NAME=PROJECT_NAME
        )
    )