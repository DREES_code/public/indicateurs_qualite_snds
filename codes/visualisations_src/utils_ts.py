import re

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from pandas.plotting import register_matplotlib_converters

register_matplotlib_converters()


def reformat_month(x):
    one_number_month = re.search("-(\d)-", x)
    if one_number_month is not None:
        x = re.sub("-(\d)-", "-0" + one_number_month.group(1) + "-", x)
    x = pd.to_datetime(re.sub("^00", "01", x), format="%d-%m-%Y")
    return x


def plot_monthly_ts(df, var, x_start=None, x_end=None, figsize=(15, 8)):
    assert var in df.columns, "{} variable not in computed indicators. Please choose one of {}".format(
        var, df.columns)
    nb_null_values = 0
    if sum(df['month_year'] == '00') != 0:
        nb_null_values = df.loc[df['month_year'] == '00', var].values[0]
    df = df.loc[df['month_year'] != '00', :]
    print("Indicateur {}: There are {} {} events related to no dates".format(var, nb_null_values,
                                                                             var))

    df["date"] = df["month_year"].map(lambda x: reformat_month(x))

    x = df["date"]
    y = df[var]
    # dates range
    if x_start is None:
        x_start = min(x)
    else:
        try:
            x_start = pd.to_datetime(x_start, format="%d-%m-%Y")
        except ValueError as error:
            print(
                "date_range start '{}' is not a date of the format dd-mm-yyyy, please enter a valid date".format(
                    x_start))
            raise
    if x_end is None:
        x_end = max(x)
    else:
        try:
            x_end = pd.to_datetime(x_end, format="%d-%m-%Y")
        except ValueError as error:
            print(
                "date_range end '{}' is not a date of the format dd-mm-yyyy, please enter a valid date".format(
                    x_end))
            raise

    df = df.loc[(df['date'] >= x_start) & (df['date'] <= x_end), :]
    fig = plt.figure(figsize=figsize)
    ax = sns.lineplot(data=df, x='date', y=var)
    axes = ax.axes
    axes.set_xlim(x_start, x_end)
    plt.xticks(rotation=45)
    plt.title("Courbe mensuelle de {}".format(var))
    plt.show()
    return ax


def plot_monthly_data(path2data, x_start=None, x_end=None, figsize=(15, 8)):
    # remove warning for setting a new column
    pd.options.mode.chained_assignment = None
    df = pd.read_csv(path2data)
    for var in df.columns:
        if var != "month_year":
            plot_monthly_ts(df, var, x_start=x_start, x_end=x_end, figsize=figsize)
