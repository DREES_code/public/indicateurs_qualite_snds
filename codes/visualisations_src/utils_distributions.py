import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns


def plot_yearly_ts(
        df,
        error_threshold=5,
        x_min=None,
        x_max=None,
        filename='',
        plot_type='scatter'):
    indicator = [val for val in df.columns if val not in ['year']][0]

    assert plot_type in ['scatter', 'lineplot', 'bar'], \
        'Argument plot_type shoudl be in ["scatter", "lineplot", "bar"]'

    removed_as_error = df.loc[(df[indicator] <= error_threshold), :]
    df = df.loc[~(df[indicator] <= error_threshold), :]
    df = df.sort_values('year', inplace=False)

    def_x_min, def_x_max = np.quantile(df["year"], [0.01, 0.99])
    if x_min is None:
        x_min = def_x_min
    if x_max is None:
        x_max = def_x_max

    df = df.loc[(df['year'] >= x_min) & (df['year'] <= x_max), :]
    fig = plt.figure(figsize=(15, 8))
    ax = None
    if plot_type == 'scatter':
        ax = plt.scatter(x=df["year"], y=df[indicator])
    elif plot_type == 'lineplot':
        ax = sns.lineplot(x=df["year"], y=df[indicator])
    elif plot_type == 'bar':
        ax = plt.bar(x=df['year'], height=df[indicator])

    plt.xlabel("year")
    plt.ylabel(df.columns[1])
    plt.title("Distribution de l'indicateur {} dans {}".format(indicator, filename))
    plt.show()
    return ax


def plot_count_distribution_by_year(
        df,
        years=None,
        filename="",
        filter_in_range=None,
        plot_type='bar',
        xticks_frequency=1.0,
        verbose=True):
    indicator = [val for val in df.columns if val not in ['year', 'count']][0]
    df = df.sort_values(by=indicator)
    assert plot_type in ['scatter', 'bar', 'lineplot', 'grouped_bars'], \
        'Argument plot_type should be in ["scatter", "bar", "lineplot", "grouped_bars"]'

    if years is not None:
        assert type(years) == list, \
            "Please enter a list"
        if type(years) == np.ndarray:
            years = years.tolist()
        df = df.loc[df["year"].isin(years), :]

    if filter_in_range is not None:
        assert type(filter_in_range) == tuple
        if verbose:
            filtered_left = df.loc[df[indicator] < filter_in_range[0], :]
            filtered_right = df.loc[df[indicator] > filter_in_range[1], :]
            print(
                'Filtered left : {} outliers with mean value {:.2f} and {}/{} total counts'.format(
                    filtered_left.shape[0],
                    np.mean(filtered_left[indicator]),
                    filtered_left['count'].sum(),
                    df['count'].sum()))
            print(
                'Filtered right : {} outliers with mean value {:.2f} and {}/{} total counts'.format(
                    filtered_right.shape[0],
                    np.mean(filtered_right[indicator]),
                    filtered_right['count'].sum(),
                    df['count'].sum()))
        df = df.loc[
             (df[indicator] >= filter_in_range[0]) & (df[indicator] <= filter_in_range[1]), :]

    if verbose:
        nb_nas = df.loc[df[indicator].isna(), 'count'].sum()
        if nb_nas != 0:
            print('WARNING: {}/{} Nas discarded '.format(
                nb_nas,
                df['count'].sum()
            ))
    sns.set(style='ticks')
    fig = plt.figure(figsize=(15, 8))
    fg = sns.FacetGrid(data=df, hue='year', height=8, aspect=1.6)
    if plot_type == 'scatter':
        fg.map(plt.scatter, indicator, 'count', alpha=0.4).add_legend()
    elif plot_type == 'bar':
        fg.map(plt.bar, indicator, 'count', alpha=0.4).add_legend()
    elif plot_type == 'grouped_bars':
        sns.barplot(data=df, x=indicator, y='count', hue='year', alpha=1)
    elif plot_type == 'lineplot':
        fg.map(sns.lineplot, indicator, 'count', alpha=0.8).add_legend()

    if False not in [str(val).isnumeric() for val in df[indicator].values]:
        plt.xticks(np.arange(min(df[indicator]), max(df[indicator]) + 1, xticks_frequency),
                   rotation=90)
    plt.title("Distribution de l'indicateur {} dans {}".format(indicator, filename))
    plt.plot()
    return fg


def plot_distribution_by_year(
        df,
        years=None,
        filename="",
        filter_in_range=None,
        plot_type='distribution',
        xticks_frequency=1.0,
        verbose=True):
    indicator = [val for val in df.columns if val not in ['year']][0]

    assert plot_type in ['distribution', 'bar', 'distribution_bar'], \
        'Argument plot_type shoudl be in ["distribution", "bar"]'
    if years is not None:
        assert (type(years) == list), \
            "Please enter a list"
        if type(years) == np.ndarray:
            years = years.tolist()
        df = df.loc[df["year"].isin(years), :]

    if filter_in_range is not None:
        assert type(filter_in_range) == tuple
        if verbose:
            filtered_left = df.loc[df[indicator] < filter_in_range[0], :]
            filtered_right = df.loc[df[indicator] > filter_in_range[1], :]
            print('Filtered left : {}/{} outliers with mean value {:.2f}'.format(
                filtered_left.shape[0],
                df.shape[0],
                np.mean(filtered_left[indicator])))
            print('Filtered right : {}/{} outliers with mean value {:.2f}'.format(
                filtered_right.shape[0],
                df.shape[0],
                np.mean(filtered_right[indicator])))
        df = df.loc[
             (df[indicator] >= filter_in_range[0]) & (df[indicator] <= filter_in_range[1]), :]

    sns.set(style='ticks')
    fig = plt.figure(figsize=(15, 8))
    fg = sns.FacetGrid(data=df, hue='year', height=8, aspect=1.6)
    if plot_type == 'distribution':
        fg.map(sns.distplot, indicator, hist=False).add_legend()
    if plot_type == 'distribution_bar':
        fg.map(sns.distplot, indicator).add_legend()
    elif plot_type == 'bar':
        fg.map(plt.hist, indicator, alpha=0.4).add_legend()
    if False not in [str(val).isnumeric() for val in df[indicator].values]:
        plt.xticks(np.arange(min(df[indicator]), max(df[indicator]) + 1, xticks_frequency),
                   rotation=90)
    plt.title("Distribution de l'indicateur {} dans {}".format(indicator, filename))
    plt.plot()
    return fg
