# Instructions

## Prérequis
- Ces codes permettent de calculer des indicateurs généraux à partir du SNDS. Ils s'appuient sur une première reformulation du SNDS où l'on a aplati la base par produit. Il y a une table pour le DCIR, une table pour le MCO, ... Ainsi on a rassemblé les tables affiliés autour des tables principales de chaque produit.
- Nous utilisons `pyspark` afin de distribuer efficacement les calculs tout en conservant une syntaxe simple et accessible.

## Usage

- Installer les librairies python nécessaires: `pip install requirements.txt`

- Indiquer dans `constants.py` le nom de votre dossier projet, le chemin vers vos données et vers votre client spark

- Lancer le main qui execute tous les notebooks `python main.py`

Les tables devraient être créées les unes après les autres par chaque notebook. 

*Nb:* Au lieu d'utiliser papermill, vous pouvez simplement utiliser jupyter et excuter les notebooks à la main.