# Résultats des indicateurs qualités sur le portail CNAM 

+ **tables:** les tables des indicateurs extraites depuis le portail SNDS de la CNAM avec les codes SAS présents dans le dossier [codes/extraction_portail_SAS](codes/pyspark_notebooks)
+ **visualisations:** Notebooks de visualisation des indicateurs. Les notebooks font appels aux fonctions de visualisation écrites dans `utils_distributions.py` et `utils_ts.py`.

## La base de calcul

La base sur laquelle sont calculés ces résultats est la base [SNDS](https://documentation-snds.health-data-hub.fr/introduction/snds.html) exhaustive de la CNAM avec une profondeur historique de 6 ans (2013-2018).