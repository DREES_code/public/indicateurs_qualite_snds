import os
import re

import pandas as pd

path2mensuels_flux = '../tables/mensuels_flux/COMPTES_MENSUELS_DCIR_FLUX.csv'
path2mensuels_pmsi = '../tables/mensuels/COMPTES_MENSUELS_PMSI.csv'
path2tables = '../tables/'


def reformat_portail_date(x):
    grep_format = re.search('(\d\d\d\d)(\d\d)', str(x))
    if grep_format is not None:
        return '00-' + grep_format.group(2) + "-" + grep_format.group(1)
    else:
        return str(x)


# convert to ',' separator
csv_files = []
for root, dirs, files in os.walk(path2tables):
    if len(files) != 0:
        if re.search('.csv', files[0]) is not None:
            for f in files:
                csv_files.append(os.path.join(root, f))

for f in csv_files:
    df = pd.read_csv(f, sep=None, engine='python')
    df.to_csv(f, index=False)

# reformat dates for monthly files
df = pd.read_csv(path2mensuels_flux)
df['month_year'] = df['month_year'].map(lambda x: reformat_portail_date(x))
df.to_csv(path2mensuels_flux, index=False)

df = pd.read_csv(path2mensuels_pmsi)
df['month_year'] = df['month_year'].map(lambda x: reformat_portail_date(x))
df.to_csv(path2mensuels_pmsi, index=False)
