import re

import pandas as pd

pd.options.mode.chained_assignment = None


def reformat_month(x):
    one_number_month = re.search("-(\d)-", x)
    if one_number_month is not None:
        x = re.sub("-(\d)-", "-0" + one_number_month.group(1) + "-", x)
    x = pd.to_datetime(re.sub("^00", "01", x), format="%d-%m-%Y")
    return x


def get_monthly_data(df, var):
    assert var in df.columns, "{} variable not in computed indicators. Please choose one of {}".format(
        var, df.columns)
    nb_null_values = df.loc[df["month_year"] == "00", var].values[0]
    df = df.loc[df["month_year"] != "00", :]
    print("Indicateur {}: There are {} {} events related to no dates".format(var, nb_null_values,
                                                                             var))

    df["date"] = df["month_year"].map(lambda x: reformat_month(x))
    df_to_plot = df.loc[:, ["date", var]]
    df_to_plot = df_to_plot.set_index("date")
    df_to_plot.sort_index(inplace=True)
    return df_to_plot
