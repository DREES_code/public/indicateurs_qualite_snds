# Résultats des indicateurs qualités sur l'échantillon 4m patients de la Drees

+ **tables:** les tables des indicateurs extraites depuis le serveur de calcul de la Drees avec les codes pyspark présents dans le dossier [codes/pyspark_notebooks](codes/pyspark_notebooks)
+ **visualisations:** Notebooks de visualisation des indicateurs. Les notebooks font appels aux fonctions de visualisation écrites dans `utils_distributions.py` et `utils_ts.py`.
+ **interactive_visu:** une tentative de vue interactive avec bokeh (TODO)

## La base de calcul

La base sur laquelle sont calculés ces résultats est une extraction [SNDS](https://documentation-snds.health-data-hub.fr/introduction/snds.html) de 4 millions de bénéficiaires avec une profondeur historique de 9 ans (2008-2016).