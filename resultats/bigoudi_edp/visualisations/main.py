import os
import re

import papermill as pm

INPUT_DIR = '../../../codes/visualisations_src/'
OUTPUT_DIR = '.'

# filter in notebook files
notebooks_filenames = [f for f in os.listdir(INPUT_DIR) if re.search('.ipynb$', f) is not None]

# filter out exceptions (depending on source-plateforme)
EXCEPTIONS = []

notebooks_filenames = [f for f in notebooks_filenames if f not in EXCEPTIONS]

if not os.path.isdir(OUTPUT_DIR):
    os.mkdir(OUTPUT_DIR)

for note in notebooks_filenames:
    print(note)
    pm.execute_notebook(
        os.path.join(INPUT_DIR, note),
        os.path.join(OUTPUT_DIR, note),
        parameters=dict(
            DEFAULT_YEARS=[2008, 2010, 2012, 2014, 2016, 2018, 2020], 
            XSTART='01-01-2008',
            QUERIED_TABLES=['DCIR', 'IRBENR', 'IRIMBR', 'MCO', 'SSR', 'HAD']
            )
    )
