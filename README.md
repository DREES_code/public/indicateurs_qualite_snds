# Indicateurs Qualité extractions SNDS
Ce dépôt fournit les codes nécessaires afin de calculer des indicateurs de qualités sur des extractions du [SNDS](https://documentation-snds.health-data-hub.fr/introduction/snds.html). Il contient aussi du code afin de produire des visualisations automatiques de ces indicateurs.

Ces indicateurs peuvent être utilisés pour apprécier la qualité des données disponibles dans une extraction de différentes façons :
- si l'extraction doit être **représentative** des données présentes sur le portail, il est possible de comparer les résultats obtenus aux résultats tirés des données du SNDS mises à disposition sur le portail de la CNAM;
- si l'extraction est **spécifique à une thématique**, les indicateurs peuvent s'utiliser de manière autonome. Elles peuvent par exemple permettre d'évaluer la stabilité des données dans le temps au sein même des données extraites, ou encore de repérer des valeurs aberrantes.

**Attention:**
*Les indicateurs fournis ici ne donnent qu'une vue générale de la base et ne constituent en aucun cas des indicateurs métiers sur l'offre de soin. Pour des études spécifiques, il est conseillé de suivre les formations de la CNAM permettant d'identifier clairement les évènements médicaux nécessaires à une étude.*



## Contenus
+ [codes](/codes/): Code source du calcul des indicateurs
  + [pyspark_notebooks](/codes/pyspark_notebooks/): implémentation pyspark sur le serveur de calcul de la drees
  + [extraction_portail_SAS](/codes/extraction_portail_SAS): implémentation sas sur le portail cnam
  + [visualisations_src](/codes/visualisations_src/) : Notebooks modèles des rapports de visualisation
+ [liste_indicateurs](/liste_indicateurs/): Liste des indicateurs calculés par les codes sources
+ [resultats](/resultats): Tables et visualisations des résultats sur deux extractions
  + [bigoudi_echantillon](/resultats/bigoudi_echantillon/): Résultats calculés sur l'extraction échantillon-profondeur du DCIR de la Drees (4 millions de patients sur 10 ans)
  + [DCIR_portailCNAM](/resultats/DCIR_portailCNAM/): Résultats calculés sur le DCIR du portail CNAM
  
## Usage

### Visualisations

On utilise [papermill](https://github.com/nteract/papermill) afin de construire toutes les visualisations à partir des notebooks. Papermill est une librairie qui permet de paramétriser, exécuter et analyser des notebooks [jupyter](https://jupyter.org/). 
On peut ainsi construire facilement toutes les visualisations puis interagir avec dans un environnement jupyter si l'on veut changer certains paramètres des vues (ex : changer les années).

#### Projet existant
+ Installer les librairies python nécessaires: `pip install requirements.txt`
+ Se placer dans le dossier visualisations du projet en question
+ Lancer `python main.py`
+ Ouvrir les rapports avec jupyter pour intéragir avec les graphiques: `jupyter-lab` puis aller sur l'URL `http://localhost:8888`.

#### Pour un nouveau projet:
+ Installer les librairies python nécessaires: `pip install requirements.txt`
+ Créer un dossier `mon_projet` dans resultats qui servira de dossier racine au nouveau projet
+ Créer un dossier `mon_projet/tables` avec une arborescence de dossier semblable à celles déjà existantes (annuels, annuels_flux, etablissements, individuels, mensuels, mensuels_flux, pse). 
+ Peupler ces dossiers avec les tables calculées à l'aide des [codes sources](/codes/).
+ Créer un dossier `mon_projet/visualisations` et se placer dedans (`cd mon_projet/visualisations`)
+ Créer un `main.py` qui permet d'automatiser les indicateurs à calculer (cf. [cet exemple de `main.py`](/resultats/DCIR_portailCNAM/visualisations/main.py))
+ Lancer le main: `python main.py`, celui-ci va construire dans le dossier tous les notebooks de visualisations. On peut ensuite interagir avec les graphes en lançant jupyter.


### Pyspark

### SAS 


### Visualisation interactive (TODO)
